# Unity 2D Topdown Steering Behaviour for Boids

Simulating flocking creatures in Unity2D, based on Craig Reynolds' research on ["Steering Behaviours For Autonomous Characters"](https://www.red3d.com/cwr/steer/) (also known as ["Boids"](https://www.red3d.com/cwr/boids/)).

## Features

Right now, only the following steering behaviours are implemented:

- Cohesion (steer towards the average position of local flockmates)
- Alignment (steer towards the average heading of local flockmates)
- Separation (steer to avoid crowding of local flockmates)
- Seek (steer towards target seek position)
- Obstacle avoidance (steer away from obstacles)
- Arrival (slowing down when approaching target position)
- Path following (steer around a dedicated path)

## Contributors

Tan Kang Soon (Joysteak Studios, developer behind Songbird Symphony)

(If you wish to contribute to the project, please send an email to kangsoon@joysteak.com)

## Install

Find `Packages/manifest.json` in your project and edit it to look like this:

```json
{
    "dependencies": {
        "com.kstan.topdown.steering-boids-2d": "https://gitlab.com/kstan/unity-2d-topdown-steering-behaviour-for-boids.git",
        ...
    },
}
```

## How to Use

### Boid.cs

`Boid` is the component that should be attached to every Boid agent in your scene. It consists of the following public members (public variables can be set via the Unity inspector or through script):

- `void UpdateBoid()`: Move the Boid according to calculated SteeringVelocity

- `Vector2 CalculateSteeringVelocity()`: Calculates and returns supposed SteeringVelocity of the boid

- `void SetTargetPosition(Vector2)`: Sets the "seek" target position that the boid will move towards

- `float [SteeringBehaviour]Weight`: Determines the weight of this steering behaviour relative to other steering behaviours. If you wish to turn off certain steering behaviours, simply set its weight to 0. For example, to stop "path following", set "PathFollowingWeight" to 0.

- `float DriveFactor`: Sets the force that propels the boid forward

- `float MaxSpeed`: Sets the maximum speed of this boid

- `float SlowDownDistance`: Sets how far from target should boid start slowing down (for Arrival)

- `float NeighbourRadius`: Sets detecting radius when finding nearby neighbours

- `float AvoidanceRadiusMultiplier`: Sets the radius within which nearby neighbours should be avoided (for Separation)

- `LayerMask AvoidLayers`: Layers to avoid, **but do not include other boids' layers** (for Obstacle Avoidance)

- `float LookAhead`: Sets the look ahead distance (for Obstacle Avoidance)

- `float PathRadius`: Sets the radius of the path that the boid is following (for Path following)

- `float Elasticity`: The smaller the number, the stronger the pull to the main path (for Path following)

- `Vector3[] Waypoints`: The list of waypoints the boid should follow (for Path following)

- `[READONLY] SteeringVelocity`: The current steering velocity of the Boid

## To-dos

- Optimise neighbour detection using KD/BSP trees

- Multi-threaded calculations of steering velocities

- "Wander" steering behaviour
    
## Further Reading

- [Board To Bits Games' tutorial](https://www.youtube.com/watch?v=i_XinoVBqt8)
- [Understanding Steering Behaviours, by GameDevTutPlus](https://gamedevelopment.tutsplus.com/series/understanding-steering-behaviors--gamedev-12732)
- [The Nature of Code, Chapter 6: Autonomous Agents](https://natureofcode.com/book/chapter-6-autonomous-agents/)

## License

Copyright (c) 2019 Tan Kang Soon. All rights reserved.

This work is licensed under the terms of the MIT license. Check LICENSE for more details.