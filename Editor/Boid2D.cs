﻿// (c) 2019 Kang Soon, Tan
// This code is licensed under MIT license (see LICENSE.txt for details)

using System.Collections.Generic;
using UnityEngine;

namespace KSTan.TopDown.SteeringBoids2D
{
    /// <summary>
    /// Boid script should be attached to a boid agent.
    /// This script calculates vectors that determins a flock-like steering behaviour.
    /// This script also moves the Boid according to the calculated steering velocity.
    /// </summary>
    [RequireComponent(typeof(BoxCollider2D))]
    [RequireComponent(typeof(Rigidbody2D))]
    public class Boid2D : MonoBehaviour
    {
        [Tooltip("Layers to avoid (do not include boids' layers)")]
        public LayerMask AvoidLayers;

        [Header("Basic Movement")]
        [Tooltip("Driving force that propels the boid forward")]
        [Range(1f, 100f)]
        public float DriveFactor = 10f;
        [Tooltip("Maximum speed of this boid")]
        [Range(1f, 100f)]
        public float MaxSpeed = 6f;
        [Tooltip("How far from destination should boid start slowing down")]
        [Range(0f, 100f)]
        public float SlowDownDistance = 5;

        [Header("Separation and obstacle avoidance")]
        [Tooltip("Detecting radius when finding nearby neighbours")]
        [Range(1f, 10f)]
        public float NeighbourRadius = 2f;
        [Tooltip("Radius within which nearby neighbours should be avoided")]
        [Range(0f, 1f)]
        public float AvoidanceRadiusMultiplier = 0.8f;
        [Tooltip("Look ahead distance for physics obstacle avoidance")]
        [Range(0f, 5f)]
        public float LookAhead = 5;

        [Header("Path following behaviours")]
        [Tooltip("Radius of the path that the boid is following")]
        [Range(0f, 5f)]
        public float PathRadius = 0.5f;
        [Tooltip("The smaller the number, the stronger the pull to the main path")]
        [Range(0f, 50f)]
        public float Elasticity = 10f;
        [Tooltip("The list of waypoints the boid should follow")]
        public Vector3[] Waypoints;

        [Header("Flocking rules' weights")]
        [Tooltip("Defines object's 'stickiness' to one another, as compared to other steering rules")]
        [Range(0f, 10f)]
        public float CohesionWeight = 5;
        [Tooltip("Defines object's alignment to one another, as compared to other steering rules")]
        [Range(0f, 10f)]
        public float AlignmentWeight = 1;
        [Tooltip("Defines object's 'separation' from one another, as compared to other steering rules")]
        [Range(0f, 10f)]
        public float SeparationWeight = 1;
        [Tooltip("Defines how much object should head towards the destination, as compared to other steering rules")]
        [Range(0f, 1f)]
        public float SeekingWeight = 0.2f;
        [Tooltip("Defines object's separation from other physics objects, as compared to other steering rules")]
        [Range(0f, 10f)]
        public float ObstacleAvoidanceWeight = 5;
        [Tooltip("Defines how much object should stick to it's path, as compared to other steering rules")]
        [Range(0f, 100f)]
        public float PathfollowingWeight = 0;
        [Tooltip("Defines how much object should be wandering, as compared to other steering rules")]
        [Range(0f, 1f)]
        public float WanderingWeight = 0.2f;

        public Vector2 SteeringVelocity { get { return _steeringVelocity;} }
        public Vector2 TargetPosition { get {return _targetPosition;} }

        /// The velocity of the Boid. We can also use this to find travelling direction
        Vector2 _steeringVelocity;
        /// The target destination of the boid
        Vector2 _targetPosition;

        float _squareMaxSpeed;
        float _squareNeighbourRadius;
        float _squareAvoidanceRadius;
        BoxCollider2D _collider;
        Rigidbody2D _rigidBody;

        /// <summary>
        /// Initialisation
        /// </summary>
        void Awake()
        {
            _collider = GetComponent<BoxCollider2D>();
            _rigidBody = GetComponent<Rigidbody2D>();

            _squareMaxSpeed = MaxSpeed * MaxSpeed;
            _squareNeighbourRadius = NeighbourRadius * NeighbourRadius;
            _squareAvoidanceRadius = _squareNeighbourRadius * AvoidanceRadiusMultiplier * AvoidanceRadiusMultiplier;

            _steeringVelocity = transform.up;

            _rigidBody.gravityScale = 0;
            _rigidBody.constraints = RigidbodyConstraints2D.FreezeRotation;
            _rigidBody.angularDrag = 0;
            _rigidBody.drag = 0;
        }

        /// <summary>
        /// Setting the seek target position
        /// </summary>
        public void SetTargetPosition(Vector2 targetPosition)
        {
            _targetPosition = targetPosition;
        }

        /// <summary>
        /// Update boid movement
        /// </summary>
        public void UpdateBoid()
        {
            _rigidBody.velocity = CalculateSteerVelocity();
        }

        /// <summary>
        /// Getting nearby objects based on neighbour radius
        /// </summary>
        List<Boid2D> GetNearbyBoids()
        {
            List<Boid2D> contacts = new List<Boid2D>();
            Collider2D[] contextColliders = Physics2D.OverlapCircleAll(transform.position, NeighbourRadius);

            for (int i = 0; i < contextColliders.Length; i++)
            {
                Collider2D c = contextColliders[i];

                // Not your own collider and the other collider is also a boid
                Boid2D otherBoid = c.GetComponent<Boid2D>();
                if (c != _collider && otherBoid != null)
                {
                    contacts.Add(otherBoid);
                }
            }

            return contacts;
        }

        /// <summary>
        /// Calculating vector for cohesion behaviour
        /// </summary>
        Vector2 CalculateCohesion(List<Boid2D> nearbyBoids)
        {
            // defaults to zero vector
            Vector2 cohesion = Vector2.zero;

            if (CohesionWeight == 0)
                return cohesion;

            // if there are no nearby objects, just return zero vector
            if (nearbyBoids.Count <= 0)
                return cohesion;

            // add all positions of nearby boids that shares the same "objectives" as you
            for (int i = 0; i < nearbyBoids.Count; i++)
            {
                bool bothAreWandering = WanderingWeight > 0 && nearbyBoids[i].WanderingWeight > 0;
                bool bothAreSeeking = SeekingWeight > 0 && nearbyBoids[i].SeekingWeight > 0;
                bool bothHaveSameTarget = _targetPosition == nearbyBoids[i].TargetPosition;
                bool bothAreSeekingSameTarget = bothAreSeeking && bothHaveSameTarget;

                if (bothAreWandering || bothAreSeekingSameTarget)
                    cohesion += (Vector2) nearbyBoids[i].transform.position;
            }

            cohesion /= nearbyBoids.Count; // get average position
            cohesion -= (Vector2) transform.position; // create offset from agent position
            Vector2 cohesionVelocity = Vector2.zero; // we need to get cohesion velocity to change smoothly with time
            float boidSmoothTime = 0.5f; // time it takes for cohesion velocity to reach cohesion vector
            cohesion = Vector2.SmoothDamp(_steeringVelocity.normalized, cohesion, ref cohesionVelocity, boidSmoothTime);
            cohesion *= CohesionWeight;

            // make sure that this behaviour is limited to the extents of the weight
            if (cohesion != Vector2.zero && cohesion.sqrMagnitude > CohesionWeight * CohesionWeight)
            {
                cohesion.Normalize();
                cohesion *= CohesionWeight;
            }

            return cohesion;
        }

        /// <summary>
        /// Calculating vector for alignment behaviour
        /// </summary>
        Vector2 CalculateAlignment(List<Boid2D> nearbyBoids)
        {
            // defaults to direction that the boid is facing right now
            Vector2 alignment = _steeringVelocity.normalized;

            // if there are no nearby objects, just return current facing direction
            if (nearbyBoids.Count <= 0)
                return alignment;

            // add all facing directions of nearby boids that share the same "objectives" as you
            for (int i = 0; i < nearbyBoids.Count; i++)
            {
                bool bothAreWandering = WanderingWeight > 0 && nearbyBoids[i].WanderingWeight > 0;
                bool bothAreSeeking = SeekingWeight > 0 && nearbyBoids[i].SeekingWeight > 0;
                bool bothHaveSameTarget = _targetPosition == nearbyBoids[i].TargetPosition;
                bool bothAreSeekingSameTarget = bothAreSeeking && bothHaveSameTarget;

                if (bothAreWandering || bothAreSeekingSameTarget)
                    alignment += (Vector2) nearbyBoids[i].SteeringVelocity.normalized;
            }

            // get average facing direction
            alignment /= nearbyBoids.Count;
            alignment *= AlignmentWeight;

            // make sure that this behaviour is limited to the extents of the weight
            if (alignment != Vector2.zero && alignment.sqrMagnitude > AlignmentWeight * AlignmentWeight)
            {
                alignment.Normalize();
                alignment *= AlignmentWeight; 
            }

            return alignment;
        }

        /// <summary>
        /// Calculating vector for steering behaviour
        /// </summary>
        Vector2 CalculateSeparation(List<Boid2D> nearbyBoids)
        {
            // defaults to zero vector
            Vector2 separation = Vector2.zero;

            if (SeparationWeight == 0)
                return separation;

            // number of things to avoid
            int numberOfAvoids = 0;

            // add the difference between your position and positions of nearby objects that are within avoidance radius
            for (int i = 0; i < nearbyBoids.Count; i++)
            {
                Transform item = nearbyBoids[i].transform;
                if (Vector2.SqrMagnitude(item.position - transform.position) < _squareAvoidanceRadius)
                {
                    numberOfAvoids ++;
                    separation += (Vector2)(transform.position - item.position);
                }
            }

            // get average separation
            if (numberOfAvoids > 0) separation /= numberOfAvoids;
            separation *= SeparationWeight;

            // make sure that this behaviour is limited to the extents of the weight
            if (separation != Vector2.zero && separation.sqrMagnitude > SeparationWeight * SeparationWeight)
            {
                separation.Normalize();
                separation *= SeparationWeight; 
            }

            return separation;
        }

        /// <summary>
        /// Calculating vector for seeking behaviour
        /// </summary>
        Vector2 CalculateSeek(List<Boid2D> nearbyBoids)
        {
            // defaults to zero vector
            Vector2 seek = Vector2.zero;

            if (SeekingWeight == 0)
                return seek;

            // Calculating the seek vector
            Vector2 desiredVelocity = _targetPosition - (Vector2) transform.position;
            seek = desiredVelocity - _steeringVelocity;
            seek.Normalize();

            // if there are no nearby objects, just head towards seek destination
            if (nearbyBoids.Count <= 0)
                return seek;
            else
                seek *= SeekingWeight;

            // make sure that this behaviour is limited to the extents of the weight
            if (seek != Vector2.zero && seek.sqrMagnitude > SeekingWeight* SeekingWeight)
            {
                seek.Normalize();
                seek *= SeekingWeight;
            }

            return seek;
        }

        /// <summary>
        /// Calculating vector for obstacle avoidance behaviour (*not including other boids*)
        /// </summary>
        Vector2 CalculateObstacleAvoidance()
        {
            Vector2 obstacleAvoidance = Vector2.zero;

            if (ObstacleAvoidanceWeight == 0)
                return obstacleAvoidance;

            Vector2 ahead = _steeringVelocity.normalized * _steeringVelocity.sqrMagnitude/(MaxSpeed*MaxSpeed) * LookAhead;
            RaycastHit2D hit = Physics2D.CircleCast(transform.position, 1, ahead, ahead.magnitude, AvoidLayers);

            // If there is an obstacle ahead
            if (hit.collider != null)
            {
                Collider2D collider = hit.collider;

                // check for obstacles on the left and right, and head in the direction with no obstacles
                Vector2 aheadRight = Vector2.Perpendicular(ahead);
                Vector2 aheadLeft = Vector2.Perpendicular(ahead) * -1;

                RaycastHit2D hitRight = Physics2D.CircleCast(transform.position, 1, aheadRight, aheadRight.magnitude, AvoidLayers);
                RaycastHit2D hitLeft = Physics2D.CircleCast(transform.position, 1, aheadLeft, aheadLeft.magnitude, AvoidLayers);

                if (hitRight.collider == null)
                {
                    obstacleAvoidance = aheadRight;
                }
                if (hitLeft.collider == null)
                {
                    obstacleAvoidance = aheadLeft;
                }
                if (hitRight.collider == null && hitLeft.collider == null)
                {
                    Vector2 avoidanceRoute = (Vector2) transform.position + ahead - (Vector2) collider.bounds.center;
                    obstacleAvoidance = (avoidanceRoute - aheadLeft).magnitude < (avoidanceRoute - aheadRight).magnitude ? aheadLeft : aheadRight;
                }

                // if both directions has obstacles, we repeat the same hit detection procedure as above but using raycasts instead of circle cast
                if (hitLeft.collider != null && hitRight.collider != null)
                {
                    hitRight = Physics2D.Raycast(transform.position, aheadRight, aheadRight.magnitude, AvoidLayers);
                    hitLeft = Physics2D.Raycast(transform.position, aheadLeft, aheadLeft.magnitude, AvoidLayers);

                    if (hitRight.collider == null)
                    {
                        obstacleAvoidance = aheadRight;
                    }
                    if (hitLeft.collider == null)
                    {
                        obstacleAvoidance = aheadLeft;
                    }
                    if (hitRight.collider == null && hitLeft.collider == null)
                    {
                        Vector2 avoidanceRoute = (Vector2) transform.position + ahead - (Vector2) collider.bounds.center;
                        obstacleAvoidance = (avoidanceRoute - aheadLeft).magnitude < (avoidanceRoute - aheadRight).magnitude ? aheadLeft : aheadRight;
                    }
                }

                obstacleAvoidance.Normalize();
                obstacleAvoidance *= ObstacleAvoidanceWeight;
            }

            // make sure that this behaviour is limited to the extents of the weight
            if (obstacleAvoidance != Vector2.zero && obstacleAvoidance.sqrMagnitude > ObstacleAvoidanceWeight * ObstacleAvoidanceWeight)
            {
                obstacleAvoidance.Normalize();
                obstacleAvoidance *= ObstacleAvoidanceWeight;
            }

            return obstacleAvoidance;
        }

        /// <summary>
        /// Get the normal point of predicted location to the path
        /// </summary>
        Vector2 GetNormalPoint(Vector2 predictLocation, Vector2 pointA, Vector2 pointB)
        {
            // a = start of path to vehicle's predicted location 
            Vector2 a = predictLocation - pointA;

            // b = start of path to end of path
            Vector2 b = pointB - pointA;

            // finding the normal point using SCALAR PROJECTION
            b.Normalize();
            b *= Vector2.Dot(a,b);
            Vector2 normalPoint = pointA + b;

            return normalPoint;
        }

        /// <summary>
        /// Calculating the vector for path following behaviour
        /// </summary>
        Vector2 CalculatePathFolllowing()
        {
            Vector2 pathFollowing = Vector2.zero;

            if (PathfollowingWeight == 0)
                return pathFollowing;

            // predict where the vehicle will be in the future
            Vector2 predict = _steeringVelocity; // get current steering velocity
            predict.Normalize(); //normalise
            predict *= 2; // look a few units ahead
            Vector2 predictLocation = (Vector2) transform.position + predict; // add vector to location to find predicted location
            
            // Loop through line segments in the waypoints list and find the normal that is (1) nearest and (2) on the path
            float nearestDistance = 2147483647;
            Vector2 chosenNormal = Vector2.zero;
            for (int i = 0; i < Waypoints.Length - 1; i++)
            {
                Vector2 pointA = Waypoints[i];
                Vector2 pointB = Waypoints[i+1];

                // Get the normal point of predicted location to the path
                Vector2 normalPoint = GetNormalPoint(predictLocation, pointA, pointB);

                // distance of normal from predict location
                float distance = (normalPoint - predictLocation).sqrMagnitude;
                
                // Check if it's the nearest normal
                if (distance > nearestDistance * nearestDistance)
                    continue;

                // Check if the normal point is on the line segment
                if ((pointA - normalPoint).magnitude + (pointB - normalPoint).magnitude != (pointA - pointB).magnitude)
                    normalPoint = pointB;

                nearestDistance = distance;
                chosenNormal= normalPoint;

                // move a little further along the path and set a target
                Vector2 direction = pointB - pointA;
                direction.Normalize();
                direction *= Elasticity;
                Vector2 target = normalPoint + direction;

                // if vehicle is outside the path beyond an acceptable radius, head towards the path
                if (distance > PathRadius)
                {
                    // Calculating the seek vector
                    Vector2 desiredVelocity = target - (Vector2) transform.position;
                    pathFollowing = desiredVelocity - _steeringVelocity;
                    pathFollowing.Normalize();
                }
            }

            // make sure that this behaviour is limited to the extents of the weight
            if (pathFollowing != Vector2.zero && pathFollowing.sqrMagnitude > PathfollowingWeight * PathfollowingWeight)
            {
                pathFollowing.Normalize();
                pathFollowing *= PathfollowingWeight;
            }

            return pathFollowing;
        }

        /// <summary>
        /// Calculate the vector for wandering behaviour
        /// </summary>
        public Vector2 CalculateWandering()
        {
            Vector2 wandering = Vector2.zero;

            if (WanderingWeight == 0)
                return wandering;

            float circleRadius = 2;
            var circleCenter = _steeringVelocity.normalized;
            var displacement = Random.insideUnitCircle * circleRadius;
            displacement = Quaternion.LookRotation(_steeringVelocity) * displacement;
            wandering = circleCenter + displacement;

            wandering.Normalize();
            wandering *= WanderingWeight;

            // make sure that this behaviour is limited to the extents of the weight
            if (wandering != Vector2.zero && wandering.sqrMagnitude > WanderingWeight * WanderingWeight)
            {
                wandering.Normalize();
                wandering *= WanderingWeight;
            }

            return wandering;
        }

        /// <summary>
        /// Adding up all the calculated behaviour vectors 
        /// </summary>
        public Vector2 CalculateSteerVelocity()
        {
            List<Boid2D> nearbyBoids = GetNearbyBoids();

            _steeringVelocity += 
                CalculateWandering() +
                CalculatePathFolllowing() +
                CalculateSeek(nearbyBoids) +
                CalculateObstacleAvoidance() +
                CalculateAlignment(nearbyBoids) +
                CalculateCohesion(nearbyBoids) +
                CalculateSeparation(nearbyBoids);

            _steeringVelocity *= DriveFactor;
            
            if (_steeringVelocity.sqrMagnitude > _squareMaxSpeed)
                _steeringVelocity = _steeringVelocity.normalized * MaxSpeed;

            if (((Vector2) transform.position - _targetPosition).magnitude < SlowDownDistance && SlowDownDistance > 0 && SeekingWeight > 0)
                _steeringVelocity *= ((Vector2) transform.position - _targetPosition).magnitude / SlowDownDistance;
            
            return _steeringVelocity;
        }
    }
}
